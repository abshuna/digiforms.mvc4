namespace DigiForms.Migrations
{
    using DigiForms.EntityModels;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DigiForms.EntityModels.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DigiForms.EntityModels.ApplicationDbContext context)
        {
            //  UnitTypes
            context.FleetTypes.AddOrUpdate(
                item => item.Name,
                new FleetType() { Name = "Executive" },
                new FleetType() { Name = "Heavy Fleet" },
                new FleetType() { Name = "Light Fleet" },
                new FleetType() { Name = "Support" }
            );

            context.SaveChanges();

            List<FleetType> fleetTypes = context.FleetTypes.ToList();

            // Unit Types
            var executiveUnitType = fleetTypes.Single(x => x.Name == "Executive");
            var heavyFleetUnitType = fleetTypes.Single(x => x.Name == "Heavy Fleet");
            var lightFleetUnitType = fleetTypes.Single(x => x.Name == "Light Fleet");
            var supportUnitType = fleetTypes.Single(x => x.Name == "Support");

            context.Fleets.AddOrUpdate(
                item => item.Name,
                new Fleet() { Name = "1", Branch = "EMS", Status = "Active", FleetTypeId = lightFleetUnitType.Id },
                new Fleet() { Name = "2", Branch = "EMS", Status = "Active", FleetTypeId = lightFleetUnitType.Id },
                new Fleet() { Name = "36", Branch = "EMS", Status = "Inactive", FleetTypeId = lightFleetUnitType.Id },
                new Fleet() { Name = "D3", Branch = "ESG", Status = "Active", FleetTypeId = executiveUnitType.Id },
                new Fleet() { Name = "D4", Branch = "ESG", Status = "Active", FleetTypeId = executiveUnitType.Id },
                new Fleet() { Name = "D10", Branch = "ESG", Status = "Active", FleetTypeId = executiveUnitType.Id },
                new Fleet() { Name = "E3", Branch = "FD", Status = "Active", FleetTypeId = heavyFleetUnitType.Id },
                new Fleet() { Name = "E4", Branch = "FD", Status = "Active", FleetTypeId = heavyFleetUnitType.Id },
                new Fleet() { Name = "E27", Branch = "FD", Status = "Inactive", FleetTypeId = heavyFleetUnitType.Id },
                new Fleet() { Name = "R5", Branch = "FRO", Status = "Active", FleetTypeId = supportUnitType.Id },
                new Fleet() { Name = "R6", Branch = "FRO", Status = "Active", FleetTypeId = supportUnitType.Id },
                new Fleet() { Name = "R20", Branch = "FRO", Status = "Inactive", FleetTypeId = supportUnitType.Id }
            );

            context.SaveChanges();
        }
    }
}
