﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DigiForms.EntityModels
{
    public class FleetType
    {
        public FleetType()
        {
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Fleet Type")]
        public string Name { get; set; }
    }
}
