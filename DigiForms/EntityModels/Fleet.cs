﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DigiForms.EntityModels
{
    public class Fleet
    {
        public Fleet()
        {
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        [DisplayName("Unit")]
        public string Name { get; set; }

        [StringLength(20)]
        public string Branch { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        [DisplayName("Fleet Type")]
        public int? FleetTypeId { get; set; }
        public virtual FleetType FleetType { get; set; }
    }
}
