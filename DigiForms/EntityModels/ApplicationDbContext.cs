﻿using System.Data.Entity;

namespace DigiForms.EntityModels
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<FleetType> FleetTypes { get; set; }

        public DbSet<Fleet> Fleets { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
